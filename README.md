# Beaver-crawler

This service is a part of the "Bearer" project, which includes:
* **The scrapper & crawler**
* The Telergam bot
* The web-site
* The web API
* The mobile application

## TL;TR
Spider designed for scraping a web site contents with comics. The resulting data is saved in the database PostgreSQL.

## Documentation
Please, see content of [index.md](./docs/index.md)

## Getting Started

Install [Node + npm](https://nodejs.org/en/download/):

Install dependencies:
```sh
npm i
```

Set environment (vars):
```sh
cp .env.example .env
# edit .env file
```

Start server:
```sh
# Start server
npm run start

# Selectively set DEBUG env var to get logs
DEBUG=crawler:*
```

Refer [debug](https://www.npmjs.com/package/debug) to know how to selectively turn on logs.

Tests:

```sh
# Run Jest tests
npm run test

# Run test along with code coverage
npm run test:coverage

# Run tests on file change
npm run test:watch
```

Lint:
```sh
# Lint code with TSLint
npm run lint
```

##### Deployment

```sh
# install production dependencies only
npm run --production
```

## Logging

Universal logging library [winston](https://www.npmjs.com/package/winston) is used for logging. You can find logs in a directory logs in root of project

## Built With

| Frameworks & libraries  |
| ----------------------- |
| [Node.js][1]            |
| [Typescript][2]         |

## Authors

- **Kazymyrov Danylo @ZulusK** - _Full stack developer_ - [git][git-me]

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details


[gitlab-me]: https://gitlab.com/ZulusK/
[git-me]: https://github.com/ZulusK
[1]: https://nodejs.org
[2]: https://www.typescriptlang.org/
